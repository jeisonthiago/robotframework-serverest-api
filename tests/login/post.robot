*** Settings ***
Documentation        Cenários de testes API POST /login

Resource             ../../resources/base.resource

*** Test Cases ***
Deve realizar login com sucesso
    [Tags]         login
    
    Dado que informo usuario e senha válidos
    Quando faço uma requisição POST para /login
    Então o status code de retorno deve ser 200
    E devo visualizar a mensagem "Login realizado com sucesso"

Não deve logar com email incorreto
    [Tags]         login
    
    Dado que informo um email incorreto e senha válida
    Quando faço uma requisição POST para /login
    Então o status code de retorno deve ser 401
    E devo visualizar a mensagem "Email e/ou senha inválidos"

Não deve logar com senha incorreta
    [Tags]         login
    
    Dado que informo email válido e senha incorreta
    Quando faço uma requisição POST para /login
    Então o status code de retorno deve ser 401
    E devo visualizar a mensagem "Email e/ou senha inválidos"

Não deve logar com email e senha incorretos
    [Tags]         login
    
    Dado que informo email e senha incorretos
    Quando faço uma requisição POST para /login
    Então o status code de retorno deve ser 401
    E devo visualizar a mensagem "Email e/ou senha inválidos"

Não deve logar com email em branco
    [Tags]         login
    
    Dado que informo email em branco e senha correta
    Quando faço uma requisição POST para /login
    Então o status code de retorno deve ser 400
    E devo visualizar a mensagem "email não pode ficar em branco"

Não deve logar com senha em branco
    [Tags]         login
    
    Dado que informo usuario válido e senha em branco
    Quando faço uma requisição POST para /login
    Então o status code de retorno deve ser 400
    E devo visualizar a mensagem "password não pode ficar em branco"