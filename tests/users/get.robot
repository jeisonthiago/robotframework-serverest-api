*** Settings ***
Documentation        Cenários de testes API GET /usuarios

Resource             ../../resources/base.resource

*** Test Cases ***
Deve listar usuários cadastrados
    [Tags]         usuários
    
    Dado que faço uma requisição GET para /usuarios
    Quando o status code do GET retorna 200
    Então a lista de usuários deve ser retornada