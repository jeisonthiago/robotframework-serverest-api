*** Settings ***
Documentation        Cenários de testes API POST /usuarios

Resource             ../../resources/base.resource

*** Test Cases ***
Deve cadastrar usuário com sucesso
    [Tags]         usuários
    
    Dado que possuo dados para cadastro de usuário
    Quando faço uma requisição POST para /usuarios
    Então o status code do POST deve ser 201
    E a mensagem retornada deve ser "Cadastro realizado com sucesso"