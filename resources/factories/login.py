

def usuario():
    login_data = {
        'email': 'fester@qa.com.br',
        'password': 'fester@123',
    }
    return login_data

def email_incorreto():
    login_data = {
        'email': '401@qa.com.br',
        'password': 'fester@123',
    }
    return login_data

def senha_incorreta():
    login_data = {
        'email': 'fester@qa.com.br',
        'password': '401@123',
    }
    return login_data

def email_e_senha():
    login_data = {
        'email': '401@qa.com.br',
        'password': '401@123',
    }
    return login_data

def email_em_branco():
    login_data = {
        'email': '',
        'password': 'fester@123',
    }
    return login_data

def senha_em_branco():
    login_data = {
        'email': 'fester@qa.com.br',
        'password': '',
    }
    return login_data