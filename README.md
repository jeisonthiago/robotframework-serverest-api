# Projeto de Automação API ServeRest com Robot Framework

<p align="center">
    <a href="https://www.cypress.io">
        <picture>
            <img alt="Robot Logo" src="https://seeklogo.com/images/R/robot-framework-logo-FED576FF0B-seeklogo.com.png" width="200px">
        </picture>
    </a>
</p>
<p align="center">
    <a href="https://robotframework.org/robotframework">Documentation</a> |
    <a href="https://github.com/robotframework/robotframework/releases">Changelog</a>
</p>

## Pré-requisitos
### 🤖 Ferramentas/Tecnologias:

- [Python](https://www.python.org/downloads/)
- [Robot Framework](https://robotframework.org/)

### 🚀 Instalação com Pip 

Se você já possui o Python com pip instalado, basta executar:
```bash
pip install robotframework
```
Robot Framework requer Python 3.8 ou mais recente e também roda em PyPy . A versão mais recente que suporta Python 3.6 e 3.7 é Robot Framework 6.1.1 . Se precisar usar Python 2, Jython ou IronPython , você pode usar Robot Framework 4.1.3 .

Para instalar as demais dependências do projeto, execute o seguinte comando no terminal:

```bash
pip install -r requirements.txt
```
Este comando instalará todos os pacotes listados no arquivo requirements.txt.

## 👨🏻‍💻 Execução do projeto

- Clonar o projeto
```bash
  git clone https://gitlab.com/jeisonthiago/robotframework-serverest-api.git
```
- Executar os testes pelo CLI. O comando deve ser executando na raiz do projeto.
```bash
  robot -d ./logs tests
```
- Ao final do teste será gerado um relatório HTML contendo a execução dos testes no diretório:
```bash
  logs/log.html
  logs/report.html
```

## 🔖 Arquitetura do projeto
```
├── resources
│   ├── factories
│   │   ├── login.py
│   │   ├── users.py
│   ├── services
│   │   ├── login.resource
│   │   ├── users.resource
│   ├── steps
│   │   ├── loginSteps.resource
│   │   ├── productsSteps.resource
│   ├── base.resource
├── tests
│   ├── login
│   │   ├── post.robot
│   ├── users
│   │   ├── delete.robot
│   │   ├── get.robot
│   │   ├── post.robot
├── .gitignore
├── .gitlab-ci.yml
├── README.md
├── requirements.txt
```
